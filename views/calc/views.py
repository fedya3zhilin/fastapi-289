from typing import Annotated

from fastapi import APIRouter, Depends, Body, Header

from .schemas import CalcAction , ActionExtra

router = APIRouter(prefix="/calc", tags =["calc"])

@router.get("/add")
def calc_mull(params: CalcAction = Depends()):
    return{
        **params.model_dump(),
        "total": params.a + params.b
    }

@router.post("/mull")
def calc_mull(params: CalcAction,
              #action: Annotated[ActionExtra, Header()] = ActionExtra.ONE,
              #action: ActionExtra = Header(ActionExtra.ONE, alias="x-extra-action"),
              action: Annotated[ActionExtra, Header(alias="x-extra-action")] = ActionExtra.ONE

              ):
    extra = 1
    if action == ActionExtra.TWO:
        extra = 2
    return{
        **params.model_dump(),
        "total": params.a * params.b * extra
    }